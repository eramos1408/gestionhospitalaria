var G_ALL="Todos";

var G_DELETE_ROW="Está seguro que quiere borrar este ítem?";

var G_COPY_TO_CLIPBOARD="Copiar al portapapeles";

var G_SAVE_TO_SCV="Guardar a CSV";

var G_SAVE_TO_EXCEL="Guardar a Excel";

var G_PAYMENT_FAILED="Pago Fallido !";

var G_NO_FURTHER_TIME_SOLOTS="No hay más franjas horarias";

var G_COPY_ROW_PORTAPAPELES="Copió una fila al portapapeles";

var G_MESSAGE_SEND="Mensaje Enviado";

var G_EMAIL_FAILED="Email Fallido";

var G_NOT_SEND ="No Enviado";

var G_UPDATED="Actualizado";


var G_ADDED="Adicionado";